Documentaci&oacute;n
-

Estructura

folder/
        app/
            action/
                *-action.php
            layout/
                layout.php
            model/
                *.php
            templates/
                *.tpl
            view/
                *-view.php
        controller/
        src/