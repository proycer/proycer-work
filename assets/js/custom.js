(function($){
	"use strict";
	
	var a = $(".bg");
	a.each(function (a) {
		if ($(this).attr("data-bg")) $(this).css("background-image", "url(" + $(this).data("bg") + ")");
	});

	$('.slideshow-container').slick({
		slidesToShow: 1,
		autoplay: true,
		autoplaySpeed:3000,
		arrows: false,
		fade: true,
		cssEase: 'ease-in',
		infinite: true,
		speed:3000
	});

	function csselem() {
		$(".slideshow-container .slideshow-item").css({
			height: $(".slideshow-container").outerHeight(true)
		});
		$(".slider-container .slider-item").css({
			height: $(".slider-container").outerHeight(true)
		});
	}
	csselem();

})(jQuery);