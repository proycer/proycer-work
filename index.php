<?php

date_default_timezone_set('America/Argentina/Buenos_Aires');
setlocale(LC_ALL, 'es_ES');

include "controller/autoload.php";

/**
 * Bloqueo por pais
 */
$ip = '181.168.147.71';#getenv('REMOTE_ADDR')
$ip = sprintf("%u", ip2long($ip));
$valid = Ip::ipCountry($ip, "AR");
if (!$valid) exit("Your country is blocked!");

if (Core::$debug) {
    ini_set('display_errors', 1);
    ini_set('display_startup_errors', 1);
    error_reporting(E_ALL);
} else {
    error_reporting(0);
}

ob_start();

session_start();

Lb::start();
