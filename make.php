<?php

/**
 * Automatizador de tareas para Proycer.
 *
 * @author        John Esteban Astete
 * @version        1.0
 */
class Make
{
    /**
     * Nombre del modelo con lowercase
     * @var
     */
    public static $_modelo;

    /**
     * Nombre del modelo con uppercase
     * @var
     */
    public static $_Modelo;

    public static $Modelo;
    public static $modelo_view;
    public static $modelonew;
    public static $modeloedit;
    public static $modelo_action;

    public static $carpeta_model;

    public static $servidor = "localhost";
    public static $usuario = "root";
    public static $password = "";
    public static $nombreBD = "u612703663_net";

    /**
     * Muestra el mensaje de ayuda.
     */
    public static function printHelp()
    {
        echo "Automatizador para Proycer v1.0".PHP_EOL;
		echo "-modulo .crear modulo completo".PHP_EOL;
        echo "-migrate .actualizar base de datos de modulo".PHP_EOL;
    }

    /**
     * Se conecta a la BD, ejecuta el query y cierra la conexion.
     * @param $sql
     */
    public static function ejecutarQuery($sql)
    {
        try {
            $con = new mysqli(self::$servidor, self::$usuario, self::$password, self::$nombreBD);
            $con->set_charset("utf8_spanish_ci");

            if ($con->query($sql)) {
                echo "Base de datos \"" . self::$nombreBD . "\" actualizada! Se creo la tabla \"" . self::$_modelo . "\"" . PHP_EOL;
            } else {
                echo "Error actualizando la base de datos!" . PHP_EOL;
            }
            $con->close();
        } catch (Exception $e) {
            error_log($e->getMessage());
            exit('Error conectandose a la Base de Datos: \"' . $e->getMessage() . "\"".PHP_EOL);
        }
    }

    /**
     * Dado los parametros necesarios, crea el modelo
     * TODO implementar adicion de atributos
     */
    public static function makeModelo()
    {
        /**
         * Obtengo el contenido del archivo modelo base.
         */
        $contenido = file_get_contents(__DIR__ . DIRECTORY_SEPARATOR . "make" . DIRECTORY_SEPARATOR . "model");

        /**
         * Reemplazo lo necesario.
         */
        $contenido = str_replace("{Modelo}", self::$_Modelo, $contenido);
        $contenido = str_replace("{modelo}", self::$_modelo, $contenido);

        /**
         * Escribo el archivo
         */
        if (file_put_contents(self::$carpeta_model . self::$Modelo, $contenido)) {
            echo "Modelo \"" . self::$_Modelo . "\" creado exitosamente! Ruta: \"" . self::$carpeta_model . self::$Modelo . "\"" . PHP_EOL;
            return true;
        } else {
            echo "Problemas al crear el modelo!" . PHP_EOL;
            return false;
        }
    }

    /**
     * Dado los parametros necesarios, crea la vista general
     */
    public static function makeView()
    {
        /**
         * Obtengo el contenido del archivo modelo base.
         */
        $contenido = file_get_contents(__DIR__ . DIRECTORY_SEPARATOR . "make" . DIRECTORY_SEPARATOR . "modelo-view");

        /**
         * Reemplazo lo necesario.
         */
        $contenido = str_replace("{Modelo}", self::$_Modelo, $contenido);
        $contenido = str_replace("{modelo}", self::$_modelo, $contenido);

        /**
         * Escribo el archivo
         */
        if (file_put_contents(self::$carpeta_model . self::$modelo_view, $contenido)) {
            echo "Vista \"" . self::$modelo_view . "\" creada exitosamente! Ruta: \"" . self::$carpeta_model . self::$modelo_view . "\"" . PHP_EOL;
            return true;
        } else {
            echo "Problemas al crear la vista general!" . PHP_EOL;
            return false;
        }
    }

    /**
     * Dado los parametros necesarios, crea la vista para crear
     */
    public static function makeNew()
    {
        /**
         * Obtengo el contenido del archivo modelo base.
         */
        $contenido = file_get_contents(__DIR__ . DIRECTORY_SEPARATOR . "make" . DIRECTORY_SEPARATOR . "modelonew-view");

        /**
         * Reemplazo lo necesario.
         */
        $contenido = str_replace("{Modelo}", self::$_Modelo, $contenido);
        $contenido = str_replace("{modelo}", self::$_modelo, $contenido);

        /**
         * Escribo el archivo
         */
        if (file_put_contents(self::$carpeta_model . self::$modelonew, $contenido)) {
            echo "Vista new \"" . self::$modelonew . "\" creada exitosamente! Ruta: \"" . self::$carpeta_model . self::$modelonew . "\"" . PHP_EOL;
            return true;
        } else {
            echo "Problemas al crear la vista nuevos!" . PHP_EOL;
            return false;
        }
    }

    /**
     * Dado los parametros necesarios, crea la accion para crear
     */
    public static function makeAction()
    {
        /**
         * Obtengo el contenido del archivo modelo base.
         */
        $contenido = file_get_contents(__DIR__ . DIRECTORY_SEPARATOR . "make" . DIRECTORY_SEPARATOR . "modelo-action");

        /**
         * Reemplazo lo necesario.
         */
        $contenido = str_replace("{Modelo}", self::$_Modelo, $contenido);
        $contenido = str_replace("{modelo}", self::$_modelo, $contenido);

        /**
         * Escribo el archivo
         */
        if (file_put_contents(self::$carpeta_model . self::$modelo_action, $contenido)) {
            echo "Accion \"" . self::$modelo_action . "\" creada exitosamente! Ruta: \"" . self::$carpeta_model . self::$modelo_action . "\"" . PHP_EOL;
            return true;
        } else {
            echo "Problemas al crear la accion!" . PHP_EOL;
            return false;
        }
    }

    /**
     * TODO implementar
     */
    public static function makeUpdate()
    {
        /**
         * Obtengo el contenido del archivo modelo base.
         */
        $contenido = file_get_contents(__DIR__ . DIRECTORY_SEPARATOR . "make" . DIRECTORY_SEPARATOR . "modeloedit-view");

        /**
         * Reemplazo lo necesario.
         */
        $contenido = str_replace("{Modelo}", self::$_Modelo, $contenido);
        $contenido = str_replace("{modelo}", self::$_modelo, $contenido);

        /**
         * Escribo el archivo
         */
        if (file_put_contents(self::$carpeta_model . self::$modeloedit, $contenido)) {
            echo "Vista actualizar \"" . self::$modeloedit . "\" creada exitosamente! Ruta: \"" . self::$carpeta_model . self::$modeloedit . "\"" . PHP_EOL;
            return true;
        } else {
            echo "Problemas al crear la accion!" . PHP_EOL;
            return false;
        }
    }

    /**
     * Genera el modelo por defecto.
     */
    public static function run($model)
    {
        $make = new Make();

        $tmp = array();
        $make::$_modelo = strtolower($model);
        $make::$_Modelo = ucfirst(strtolower($model));

        /**
         * Rutas de los archivos
         */
        $make::$carpeta_model = __DIR__ . DIRECTORY_SEPARATOR . "app" . DIRECTORY_SEPARATOR . $make::$_modelo . DIRECTORY_SEPARATOR;

        /**
         * Nombre de los archivos a generar
         */
        $make::$Modelo = $make::$_modelo . ".php";
        $make::$modelo_view = $make::$_modelo . "-view.php";
        $make::$modelonew = $make::$_modelo . "new-view.php";
        $make::$modeloedit = $make::$_modelo . "edit-view.php";
        $make::$modelo_action = $make::$_modelo . "-action.php";

        /**
         * Bandera por si falla una creacion.
         */
        $ok = false;

        /**
         * Modifico los archivos
         */
        if (!file_exists(self::$carpeta_model)) {
            mkdir(self::$carpeta_model);

            if ($make::makeModelo()) {
                if ($make::makeView()) {
                    if ($make::makeNew()) {
                        if ($make::makeUpdate()) {
                            if ($make::makeAction()) {
                                $ok = true;
                            }
                        }
                    }
                }
            }
        } else {
            echo "Carpeta ".self::$carpeta_model." existente!".PHP_EOL;
        }

        if ($ok) {
            /**
             * Actualizo la BD con la estructura
             */
            $sql = "CREATE TABLE IF NOT EXISTS " . self::$_modelo . " (
            id INT(6) UNSIGNED AUTO_INCREMENT PRIMARY KEY,
            nombre VARCHAR(64) NOT NULL
            ) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;";

            self::ejecutarQuery($sql);
        }

        /**
         * Ingreso los atributos.
         */
        #echo "Ingresa los atributos separados por coma: ";
        #$atributos = trim(fgets(STDIN));

        /**
         * Preparo las variables.
         */
        #$atributos = explode(",", $atributos);
        #$atributos_constructor = "";

        #foreach ($atributos as $atributo) {
        #	$atributos_constructor .= '$this->'.$atributo.' = "";'.PHP_EOL;
        #}

        #$contenido = str_replace("{atributos_constructor}", $atributos_constructor, $contenido);

        #echo $atributos_constructor;

        #$resultado = file_put_contents($nombre_carpeta.$nombre_archivo, $contenido);

        #if ($resultado) echo "Realizado!";
        #else echo "No realizado!";
    }

    /**
     * Genera el modelo por defecto.
     */
    public static function migrate($model)
    {
        $make = new Make();

        $tmp = array();
        $make::$_modelo = strtolower($model);
        $make::$_Modelo = ucfirst(strtolower($model));

        /**
         * Rutas de los archivos
         */
        $make::$carpeta_model = __DIR__ . DIRECTORY_SEPARATOR . "app" . DIRECTORY_SEPARATOR . $make::$_modelo . DIRECTORY_SEPARATOR;

        /**
         * Nombre de los archivos a generar
         */
        $make::$Modelo = $make::$_modelo . ".php";
        $make::$modelo_view = $make::$_modelo . "-view.php";
        $make::$modelonew = $make::$_modelo . "new-view.php";
        $make::$modeloedit = $make::$_modelo . "edit-view.php";
        $make::$modelo_action = $make::$_modelo . "-action.php";

        /**
         * Bandera por si falla una creacion.
         */
        $ok = false;

        /**
         * Modifico los archivos
         */
        if (!file_exists(self::$carpeta_model)) {
            mkdir(self::$carpeta_model);

            if ($make::makeModelo()) {
                if ($make::makeView()) {
                    if ($make::makeNew()) {
                        if ($make::makeUpdate()) {
                            if ($make::makeAction()) {
                                $ok = true;
                            }
                        }
                    }
                }
            }
        } else {
            echo "Carpeta ".self::$carpeta_model." existente!".PHP_EOL;
        }

        if ($ok) {
            /**
             * Actualizo la BD con la estructura
             */
            $sql = "CREATE TABLE IF NOT EXISTS " . self::$_modelo . " (
            id INT(6) UNSIGNED AUTO_INCREMENT PRIMARY KEY,
            nombre VARCHAR(64) NOT NULL
            ) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;";

            self::ejecutarQuery($sql);
        }

        /**
         * Ingreso los atributos.
         */
        #echo "Ingresa los atributos separados por coma: ";
        #$atributos = trim(fgets(STDIN));

        /**
         * Preparo las variables.
         */
        #$atributos = explode(",", $atributos);
        #$atributos_constructor = "";

        #foreach ($atributos as $atributo) {
        #	$atributos_constructor .= '$this->'.$atributo.' = "";'.PHP_EOL;
        #}

        #$contenido = str_replace("{atributos_constructor}", $atributos_constructor, $contenido);

        #echo $atributos_constructor;

        #$resultado = file_put_contents($nombre_carpeta.$nombre_archivo, $contenido);

        #if ($resultado) echo "Realizado!";
        #else echo "No realizado!";
    }


    /**
     * Termina el script.
     */
    public static function error()
    {
        exit("Comando invalido!".PHP_EOL);
    }
}

/**
 * Comprueba los argumentos.
 */
switch ($cant_args = count($argv)) {
    case 2:
        if ($argv[1] == '-h') Make::printHelp();
        else Make::error();
        break;
    case 3:
        if ($argv[1] == '-modulo') Make::run($argv[2]);
        elseif ($argv[1] == '-migrate') Make::migrate($argv[2]);
        else Make::error();
        break;
    default:
        Make::error();
        break;
}
