<?php

/**
 * Class Action
 */
class Action
{
    /**
     * Carga la accion correspondiente a un modulo
     */
	public static function load()
	{
        if (isset($_GET['model']) && isset($_GET['action'])) {
            if (Action::isValid()) {
                include "app/".$_GET['model']."/".$_GET['model']."-action.php";
            } else {
                ErrorController::jsAlert("Accion erronea!");
                Core::redir("./?model=templates&view=error");
            }
        }
	}

    /**
     * Verifica la existencia de una accion
     * @return bool
     */
	public static function isValid()
	{
		if (file_exists($file = "app/".$_GET['model']."/".$_GET['model']."-action.php")) {
			return true;
		} else {
		    return false;
        }
	}
}
