<?php

/**
 * Class Core
 */
class Core
{
	public static $debug = true;

    /**
     * Almacena la variable $_POST
     * @var
     */
	public static $post;

    /**
     * @param $url
     */
	public static function redir($url)
	{
		echo "<script>window.location='".$url."';</script>";
	}

	/**
	 * Gestiona la sesion verificando el tiempo de inactividad.
	 */
	public static function timeOut()
	{
		# 1 hora 3600 segundos
		$inactividad = 3600;

		if (isset($_SESSION["timeout"])) {
			$sessionTTL = time() - $_SESSION["timeout"];
			if ($sessionTTL > $inactividad) {
                unset($_SESSION);
				session_destroy();
                ErrorController::jsAlert("Sesion finalizada!");
				self::redir('./?model=web&view=login');
			}
		}
	}
}
