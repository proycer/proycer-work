<?php

/**
 * Class AnexGrid
 */
class AnexGrid
{
    /**
     * Cantidad de registros por pagina
     * @var int
     */
    public $limite = 0;

    /**
     * Desde numero de fila que paginara
     * @var int
     */
    public $pagina = 0;

    /**
     * @var string
     */
    public $columna = '';

    /**
     * @var string
     */
    public $columna_orden = '';

    /**
     * @var array
     */
    public $filtros = array();

    /**
     * @var array
     */
    public $parametros = array();

    /**
     * AnexGrid constructor.
     */
    public function __construct()
    {
        $this->limite = $_REQUEST['limite'];
        if(!is_numeric($this->limite)) return;

        $this->pagina = $_REQUEST['pagina'] - 1;
        if(!is_numeric($this->pagina)) return;
        
        if( $this->pagina > 0) $this->pagina = $this->pagina * $this->limite;
        
        /* Ordenamiento de las filas */
        $this->columna = $_REQUEST['columna'];
        $this->columna_orden = $_REQUEST['columna_orden'];

        if(isset($_REQUEST['filtros']))
            $this->filtros = $_REQUEST['filtros'];

        if(isset($_REQUEST['parametros']))
            $this->parametros = $_REQUEST['parametros'];
    }

    /**
     * @param $data
     * @param $total
     * @return false|string
     */
    public function responde($data, $total)
    {
        #DebugController::showDebug();

        return print_r(
            json_encode(
                array(
                    'data' => $data,
                    'total' => $total
                ),
                JSON_UNESCAPED_UNICODE
            )
        );
    }

    /**
     * Devuelve la tabla de la clase solicitada
     * @return false|array
     */
    public static function listar($clase)
    {
        try
        {
            /**
             * AnexGRID
             */
            $anexgrid = new AnexGrid();

            /**
             * Los registros
             */
            $sql = "SELECT * FROM ".$clase::$tabla."
            ORDER BY $anexgrid->columna $anexgrid->columna_orden
            LIMIT $anexgrid->pagina, $anexgrid->limite";

            $result = Database::solicitarVarios($sql, new $clase());
            $total = $clase::getCantTotal();

            return $anexgrid->responde($result, $total);
        }
        catch(Exception $e)
        {
            die("Error obteniendo ".$clase::$tabla.": ".$e->getMessage());
        }
    }
}