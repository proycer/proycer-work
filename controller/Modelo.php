<?php

/**
 * Trait Modelo
 *
 * Se encarga de encapsular los metodos de un modelo
 * para ser usados por los modelos personalizados
 *
 */
trait Modelo
{
    /**
     * Agrega la instancia del modelo a la BD
     */
    public function add()
    {
        $campos = Crudadmin::prepareFields(self::$schema, "add");
        $valores = Crudadmin::prepareValues(self::$schema, $this, "add");
        $sql = Crudadmin::buildIFromFV(self::$tabla, $campos, $valores);
        Database::modificar($sql);
    }

    /**
     * Elimina la instancia del modelo de la BD
     */
    public function del()
    {
        $sql = "DELETE FROM ".self::$tabla." WHERE id = $this->id";
        Database::modificar($sql);
    }

    /**
     * Actualiza la instancia del modelo en la BD
     */
    public function update()
    {
        $campos = Crudadmin::prepareFields(self::$schema, "edit");
        $valores = Crudadmin::prepareValues(self::$schema, $this, "edit");
        $sql = Crudadmin::buildUFromFV(self::$tabla, $campos, $valores, $this->id);
        Database::modificar($sql);
    }

    /**
     * Devuelve el registro almacenado de este objeto
     * @param $id
     * @return null
     */
    public static function getById($id)
    {
        $sql = "SELECT * FROM ".self::$tabla." WHERE id = $id";
        return Database::solicitarUno($sql, new self());
    }

    /**
     * Devuelve todos los registro almacenados de este objeto
     * @return array
     */
    public static function getAll(): array
    {
        $sql = "SELECT * FROM " . self::$tabla;
        return Database::solicitarVarios($sql, new self());
    }

    /**
     * Devuelve el ultimo registro ingresado.
     */
    public static function getUltimo()
    {
        $sql = "SELECT * FROM " . self::$tabla . " ORDER BY id DESC LIMIT 1";
        return Database::solicitarUno($sql, new self());
    }

    /**
     * @return mixed
     */
    public static function getCantTotal()
    {
        $sql = "SELECT COUNT(*) FROM " . self::$tabla;
        return Database::contar($sql);
    }

    public static function vaciar()
    {
        $sql = "TRUNCATE TABLE ".self::$tabla;
        Database::modificar($sql);
    }
}
