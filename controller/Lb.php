<?php

class Lb
{
	public static function start()
	{
	    include "app/autoload.php";

		/**
		 * Control de sesion.
		 */
		if(isset($_SESSION["timeout"])) Core::timeOut();

        /**
         * Si no se pide una accion para ejecutar, carga la vista.
         */
        if (!isset($_GET["action"])) {
            if (!isset($_GET["model"]) || (isset($_GET["model"]) && $_GET["model"] == "web")) {
                include "app/layouts/frontend.php";
            } elseif (isset($_GET["model"]) && $_GET["model"] != "web") {
                include "app/layouts/backend.php";
            }
        } else {
            Action::load();
        }
	}
}
