<?php

/**
 * Class Notificaciones
 *
 * Manipula notificaciones almacenadas en la sesion.
 */
class NotificacionesController
{
    /**
     * @var string
     */
    public $estado;

    /**
     * @var string
     */
    public $tipo;

    /**
     * @var string
     */
    public $contenido;

    /**
     * @var string
     */
    public $titulo;

    /**
     * Devuelve las notificaciones generadas
     */
    public static function getNotificaciones()
    {
        if (isset($_SESSION["notificaciones"])) {
            $notificaciones = $_SESSION["notificaciones"];

            foreach ($notificaciones as $notify) {
                echo $notify;
            }
            unset($_SESSION["notificaciones"]);
        }
    }

    /**
     * Crea una notificacion
     * @param $tipo
     * @param $mensaje
     */
    public static function setNotificacion($tipo, $mensaje, $titulo)
    {
        $notify = "toastr.$tipo(\"$mensaje\", \"$titulo\", {
            \"closeButton\": true,
            \"positionClass\": \"toast-top-right\",
            \"showEasing\": \"swing\",
            \"hideEasing\": \"linear\",
            \"showMethod\": \"fadeIn\",
            \"hideMethod\": \"fadeOut\"
        });";

        if (!isset($_SESSION["notificaciones"]))
        {
            $_SESSION["notificaciones"] =  array();
        }

        array_push($_SESSION["notificaciones"], $notify);
    }
}