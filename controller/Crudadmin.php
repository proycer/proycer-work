<?php

/**
 * Class Crudadmin
 *
 * Genera automaticamente las consultas a la Base de Datos.
 */
class Crudadmin
{
    /**
     * Valida un schema de modelo.
     * TODO implementar
     * @param $schema
     * @return bool
     */
	public static function valid($schema)
	{

		return true;
	}

    /**
     * Crea un objeto en base al esquema de su modelo
     *
     * @param $schema
     * @param $model
     */
	public static function add($schema, $model)
	{
		foreach ($schema as $clave => $valor) {
			if ($valor["kind"] != "hidden" && in_array("add", explode(",", $valor["views"]))) {
				$model->{$clave} = Core::$post[$clave];
			}
		}
		$model->add();
	}

    /**
     *
     * @param $schema
     * @param $model
     * @param string $action
     */
	public static function update($schema, $model)
	{
        /**
         * Recorro el schema del modelo y le asigno los datos del post
         */
		foreach ($schema as $k =>$v) {

            /**
             * Si no es un campo oculto lo almaceno
             */
			if ( $v["kind"] != "hidden") {

                /**
                 * Si es un campo editable
                 */
			    if (in_array("edit", explode(",", $v["views"]))) {
				    $model->{$k} = Core::$post[$k];
			    }
			}
		}
		$model->update();
	}

    /**
     * Prepara un arreglo con los campos del esquema
     * @param $schema
     * @param string $action
     * @return array
     */
	public static function prepareFields($schema, $action)
	{
		$campos = array();
		foreach ($schema as $k=>$v) {
			if (in_array($action, explode(",", $v["views"]))) {
				$campos[] = $k;
			}
		}
		return $campos;
	}

    /**
     * Prepara un arreglo con las etiquetas del esquema
     * @param $schema
     * @param string $action
     * @return array
     */
	public static function prepareLabels($schema, $action)
	{
		$campos = array();
		foreach ($schema as $k=>$v) {
			if (in_array($action, explode(",", $v["views"]))) {
				$campos[] = $v["label"];
			}
		}
		return $campos;
	}

    /**
     * Prepara una arreglo con los valores del esquema
     * @param $schema
     * @param $ths
     * @param string $action
     * @return array
     */
	public static function prepareValues($schema, $ths, $action)
	{
	    $valores = array();
		foreach ($schema as $k=>$v) {
			if (in_array($action, explode(",", $v["views"]))) {

                /**
                 * Verifico si es una foto
                 */
                if (is_array($ths->{$k})) {
                    $val = $ths->$k["name"];
                } else {
                    $val = $ths->{$k};
                }

				if (!is_numeric($val) && $val != "NOW()" && $val != "NULL") {
					$val = "\"$val\"";
				}
				$valores[] = $val;
			}
		}
		return $valores;
	}

    /**
     * Crea una consulta INSERT para la BD valores dados.
     * @param $tabla
     * @param $campos
     * @param $valores
     * @return string
     */
	public static function buildIFromFV($tabla, $campos, $valores)
	{
		return "INSERT INTO ".$tabla." (".implode(",", $campos).") VALUES (".implode(",", $valores).")";
	}

    /**
     * Crea una consulta UPDATE para la BD valores dados.
     * @param $tabla
     * @param $campos
     * @param $valores
     * @param $id
     * @return string
     */
	public static function buildUFromFV($tabla, $campos, $valores, $id)
	{
		$ds = array();
        $cant_campos = count($campos);

		for ($i = 0; $i < $cant_campos; $i++) {
			$ds[] =  $campos[$i]." = ".$valores[$i];
		}

		$sql = "UPDATE ".$tabla." SET ".implode(",", $ds)." WHERE id = $id";
		return $sql;
	}

    /**
     * Crea una consulta SELECT para la BD
     * @param $tabla
     * @param $fields
     * @return string
     */
	public static function buildSelect($fields, $tabla, $conditions = null)
	{
		$sql = "SELECT $fields FROM $tabla";
		if ($conditions != null || $conditions != "") $sql .= " WHERE $conditions";
		return $sql;
	}

    /**
     * Crea una consulta SELECT COUNT para la BD
     * @param $tabla
     * @param $fields
     * @return string
     */
    public static function buildCount($tabla, $conditions = null)
    {
        $sql = "SELECT COUNT(*) FROM $tabla";
        if ($conditions != null || $conditions != "") $sql .= " WHERE $conditions";
        return $sql;
    }
}
