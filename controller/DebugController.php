<?php

/**
 * Class DebugController
 */
class DebugController
{
    /**
     * @param $arg
     */
    public static function showDebug($arg): void
    {
        echo "<pre>";
        print_r($arg);
        echo "</pre>";
    }

    /**
     * @param string $arg
     * @return void
     */
    public static function jsLog(string $arg): void
    {
        echo "<script>console.log($arg)</script>";
    }
}
