<?php

/**
 * Class View
 */
class View
{
    /**
     * Carga la vista correspondiente a un modulo
     */
    public static function load()
    {
        if (!isset($_GET["model"])) {
            Core::redir("./?model=web&view=inicio");
        } elseif (isset($_GET['model']) && !isset($_GET['view'])) {
            if (View::isValid()) {
                include "app/".$_GET['model']."/".$_GET['model']."-view.php";
            } else {
                Core::redir("./?model=templates&view=error");
            }
        } else if (isset($_GET['model']) && isset($_GET['view'])) {
            if (View::isValid()) {
                include "app/".$_GET['model']."/".$_GET['model'].$_GET['view']."-view.php";
            } else {
                Core::redir("./?model=templates&view=error");
            }
        } else {
            Core::redir("./?model=templates&view=error");
        }
    }

    /**
     * Verifica la existencia de una vista
     * @return bool
     */
	public static function isValid()
	{
        $valid = false;

        if (isset($_GET["model"]) && isset($_GET["view"])) {
            if (file_exists($file = "app/".$_GET['model']."/".$_GET['model'].$_GET["view"]."-view.php")) {
                $valid = true;
            }
        } else {
            if (file_exists($file = "app/" . $_GET['model'] ."/".$_GET['model']. "-view.php")) {
                $valid = true;
            }
        }
        return $valid;
	}
}
