<?php
/**
 * Core/controller/Database.php
 *
 * PHP Version 7.2.
 *
 * @author John Astete <john.astete@telsis.com.ar>
 */

/**
 * Maneja la conexion y consultas a la BD
 */
class Database
{
    public static $db;

    public static $con;

    private $usuario = "root";

    private $password = "";

    private $host = "localhost";

    private $database = "netdigital";

    /**
     * Conexion a la BD
     */
    function connect()
    {
        if (Core::$debug) mysqli_report(MYSQLI_REPORT_ERROR | MYSQLI_REPORT_STRICT);

        try {
            $mysqli = new mysqli($this->host, $this->usuario, $this->password, $this->database);
            $mysqli->set_charset("utf8");
            return $mysqli;
        } catch (Exception $e) {
            error_log($e->getMessage());
            exit('Error conectandose a la Base de Datos: ' . $e->getMessage());
        }
    }

    /**
     * Devuelve una conexion valida
     */
    public static function getCon()
    {
        if (self::$con == null && self::$db == null) {
            self::$db = new Database();
            self::$con = self::$db->connect();
        }
        return self::$con;
    }

    /**
     * Realiza una consulta de modificacion a la BD
     * @param $sql
     */
    public static function modificar($sql)
    {
        $con = self::getCon();
        $con->query($sql);
    }

    /**
     * Realiza una consulta de solicitud a la BD y devuelve un
     * objeto de la clase consultada. Si la consulta es negativa
     * devuelve un objeto NULL.
     */
    public static function solicitarUno($sql, $clase)
    {
        $con = self::getCon();
        $consulta = array($con->query($sql), $con->insert_id)[0];

        $objeto = new $clase;
        $i = 1;

        while ($respuesta = $consulta->fetch_array()) {

            foreach ($respuesta as $atributo => $valor) {

                if ($i % 2 == 0) {
                    $objeto->$atributo = $valor;
                }
                $i++;
            }
        }

        /**
         * Verifico la existencia del objeto
         */
        if (!isset($objeto->id)) {
            return NULL;
        } else {
            return $objeto;
        }
    }

    /**
     * Realiza una consulta de solicitud a la BD y devuelve un arreglo
     * con los objetos de la clase consultada.
     */
    public static function solicitarVarios($sql, $clase)
    {
        $con = self::getCon();
        $consulta = array($con->query($sql), $con->insert_id)[0];

        $i = 0;
        $objetos = array();

        /**
         * Recorro todos los resultados de la consulta.
         */
        while ($respuesta = $consulta->fetch_array()) {
            $objetos[$i] = new $clase;
            $j = 1;

            /**
             * Almaceno respuesta por medio.
             */
            foreach ($respuesta as $atributo => $valor) {
                if ($j > 0 && $j % 2 == 0) {
                    $objetos[$i]->$atributo = $valor;
                }
                $j++;
            }
            $i++;
        }
        return $objetos;
    }

    /**
     * Realiza una consulta a la BD y devuelve la cantidad.
     * @param string $sql
     * @return mixed
     */
    public static function contar(string $sql)
    {
        $con = self::getCon();
        $consulta = $con->query($sql);
        $respuesta = $consulta->fetch_row();
        return $respuesta[0] == 0 ? 0 : $respuesta[0];
    }

    /**
     * Devuelve false si no existe, caso contrario devuelve true
     * @param string $sql
     * @return bool
     */
    public static function exists(string $sql): bool
    {
        $con = self::getCon();
        $consulta = $con->query($sql);
        $respuesta = $consulta->fetch_row();
        return $respuesta[0] == 0 ? false : true;
    }
}
