<?php

/**
 * Class Bs
 */
class Bs
{

    /**
     * Crea un input segun los parametros dados.
     * TODO agregar placeholder y required
     * @param $label
     * @param $value
     * @param $type
     * @param $name_id
     * @param string $cl
     * @return string
     */
    public static function input($etiqueta, $valor, $tipo, $name_id, $clase = 'form-control')
    {
        $html = "";

        if ($tipo != 'hidden') {
            $html = "<div class='form-group'>";
            $html .= "<label for='$name_id' class='col-lg-3 control-label hidden-xs'>$etiqueta</label>";
            $html .= "<div class='col-md-6'>";
        }

        /**
         * Oculto el password
         */
        if ($tipo == "password") {
            $valor = "";
        }

        switch ($tipo) {
            case 'textarea':
                $html .= "<textarea name='$name_id' id='$name_id' class='$clase' placeholder='$etiqueta'>$valor</textarea>";
                break;
            case 'file':
                $html .= "<input type='$tipo' accept='image/*' name='$name_id' value='$valor' class='$clase' id='$name_id' placeholder='$etiqueta'>";
                break;
            case 'date':
                $valor == null ? $valor = date("Y-m-d") : '';
                $html .= "<input type='$tipo' name='$name_id' value='".$valor."' class='$clase' id='$name_id'>";
                break;
            default:
                $html .= "<input type='$tipo' name='$name_id' value='$valor' class='$clase' id='$name_id' placeholder='$etiqueta'>";
                break;
        }

        if ($tipo != 'hidden') {
            $html .= "</div></div>";
        }

        return $html;
    }

    /**
     * Crea un select apartir de un array de datos proporcionados
     * TODO generar el selected cuando sea una edicion
     *
     * @param $label
     * @param $name_id
     * @param $data
     * @param string $selected
     * @return string
     */
    public static function select($label, $name_id, $data, $selected = '')
    {
        $html = "<div class='form-group'>";
        $html .= "<label for='$name_id' class='col-lg-3 control-label'>$label</label>";
        $html .= "<div class='col-md-6'>";
        $html .= "<select name='$name_id' class='form-control' id='$name_id'>";

        foreach ($data as $clave => $valor) {

            $aux = '';

            if ($clave == $selected) {
                $aux = 'selected';
            }

            $html .= "<option value='$clave' $aux>$valor</option>";

        }

        $html .= "</select></div></div>";
        return $html;
    }

    /**
     * Crea un select apartir de una asociacion one to one
     * TODO generar el selected cuando sea una edicion
     *
     * @param $label
     * @param $name_id
     * @param $data
     * @param string $selected
     * @return string
     */
    public static function one2one($label, $name_id, $data, $selected = '')
    {
        $html = "<div class='form-group'>";
        $html .= "<label for='$name_id' class='col-lg-3 control-label'>$label</label>";
        $html .= "<div class='col-md-6'>";
        $html .= "<select name='$name_id' class='form-control select2' id='$name_id'>";

        $class = $data[0]::getAll();

        /**
         * Muestro en un select la lista de objetos por el atributo dado
         */
        foreach ($class as $clase) {

            $tmp = '';
            if ($selected == $clase->id) {
                $tmp = ' selected';
            }

            $html .= "<option value='$clase->id' $tmp>";

            $html .= $clase->toString();

            $html .= "</option>";
        }

        $html .= "</select></div></div>";

        return $html;
    }

    /**
     * Toma todos los campos definidos en el schema con add y crea el formulario para crear.
     *
     * @param $schema
     * @param string $views
     * @param array $custom_field
     */
    public static function render_new($schema)
    {
        foreach ($schema as $su => $sub) {

            if (in_array("add", explode(",", $sub["views"]))) {

                switch ($sub["kind"]) {
                    case "one2one":
                        echo Bs::one2one($sub["label"], $su, $sub["data"]);
                        break;
                    case "select":
                        echo Bs::select($sub["label"], $su, $sub["data"]);
                        break;
                    default:
                        echo Bs::input($sub["label"], "", $sub["kind"], $su);
                        break;
                }
            }
        }
    }

    /**
     * Toma todos los campos definidos en el schema y crea el formulario para modificar
     * TODO implementar
     *
     * @param $schema
     * @param $datamodel
     * @param string $views
     */
    public static function render_edit($schema, $datamodel)
    {
        foreach ($schema as $su => $sub) {

            if (in_array("edit", explode(",", $sub["views"]))) {

                switch ($sub["kind"]) {
                    case "one2one":
                        echo Bs::one2one($sub["label"], $su, $sub["data"], $datamodel->{$su});
                        break;
                    case "select":
                        echo Bs::select($sub["label"], $su, $sub["data"], $datamodel->{$su});
                        break;
                    default:
                        echo Bs::input($sub["label"], $datamodel->{$su}, $sub["kind"], $su);
                        break;
                }
            }
        }
    }
}
