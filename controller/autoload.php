<?php

/**
 * Cargo todos los controladores
 */
$carpeta = "controller/";
$aux = opendir($carpeta);

if ($aux) {
    while (false !== ($fichero = readdir($aux))) {
        if ($fichero != "." && $fichero != "..") {
            $fullpath = $carpeta.$fichero;
            if (!is_dir($fullpath)) {
                require_once $fullpath;
            }
        }
    }
    closedir($aux);
} else {
    DebugController::showDebug("No se puede leer carpeta controller");
}
