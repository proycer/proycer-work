<?php
/**
 * Se encarga de ralizar tareas con las IPs de los clientes,
 * tales como bloquear, registrar, etc.
 */

/**
 * Class Ip
 */
class Ip
{
    public static $tabla = "ips";

    /**
     * Devuelve verdadero si la ip dada pertenece al pais buscado
     * @param string $ip
     * @param string $country
     * @return bool
     */
    public static function ipCountry(string $ip, string $country): bool
    {
        $sql = "SELECT * FROM ".self::$tabla." WHERE ($ip BETWEEN ip_from AND ip_to) AND country_code = '$country'";
        return Database::exists($sql);
    }
}